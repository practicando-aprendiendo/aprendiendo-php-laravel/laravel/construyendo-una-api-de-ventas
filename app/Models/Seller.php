<?php

namespace App\Models;

use App\User;

class Seller extends User
{

    /*
    |--------------------------------------------------------------------------
    | Relationships
    |--------------------------------------------------------------------------    
    */

    /**
     * Un vendedor tiene muchos productos
     * 
     * @return Product
     */
    public function products()
    {
        return $this->hasMany(Product::class);
    }
}
