<?php

namespace App\Models;

use App\User;

class Buyer extends User
{

    /*
    |--------------------------------------------------------------------------
    | Relationshipts
    |--------------------------------------------------------------------------    
    */

    /**
     * Un comprador tiene muchas transacciones
     * 
     * @return Transaction
     */
    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }
}
